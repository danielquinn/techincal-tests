<div align="center">
  <img src="interview-question.png" alt="Interview question comic"><br />
  Image courtesy of Vincent Déniel (CC BY-NC 4.0 license)
</div>

# Interview Tests

I've done a lot of tests over the years.  Some of them were really good, while
others are just ridiculous exercises in math.  Regardless, I thought it a good
idea to archive them all somewhere.


## Favourites

* [Scaling Autocomplete (Assembl)](Scaling%20Autocomplete/): Write a simple autocomplete web service and make it scale like crazy.
* [Database Sharding](Database%20Sharding/): Break a database into arbitrary shards, and script it so it can be rebalanced across as many as you like.
* [Met Office Weather](Weather/): Pull down and parse an ugly dataset and paint it into something beautiful.
* [Minesweeper](Minesweeper/): A console-based implementation of the old Windows favourite.

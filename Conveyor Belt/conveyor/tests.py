from unittest import TestCase, mock

from .slot import Slot, SlotEmptyError, SlotOccupiedError
from .worker import Worker
from .belt import Belt

from .constants import PRODUCT, COMPONENT_A, COMPONENT_B


class SlotTestCase(TestCase):

    def test___init__(self):
        self.assertEqual(Slot("A").holding, "A")

    def test___str__(self):
        self.assertEqual(str(Slot("A")), "A")
        self.assertEqual(str(Slot()), " ")

    def test__bool__(self):
        self.assertTrue(Slot("A"))
        self.assertFalse(Slot())

    def test_push(self):
        slot = Slot()
        self.assertIsNone(slot.holding)
        slot.push("A")
        self.assertIsNotNone(slot.holding)
        self.assertEqual(slot.holding, "A")
        self.assertRaises(SlotOccupiedError, slot.push, "A")
    
    def test_pop(self):
        slot = Slot("A")
        self.assertIsNotNone(slot.holding)
        self.assertEqual(slot.holding, "A")
        self.assertEqual(slot.pop(), "A")
        self.assertIsNone(slot.holding)
        self.assertRaises(SlotEmptyError, slot.pop)


class WorkerTestCase(TestCase):
    
    def test___init__(self):
        self.assertEqual(Worker(None, None).holding, [None, None])
        self.assertEqual(Worker("A", None).holding, ["A", None])
        self.assertEqual(Worker("A", "B").holding, ["A", "B"])
        self.assertEqual(Worker(None, "B").holding, [None, "B"])

    def test___str__(self):
        self.assertEqual(str(Worker()), "___")
        self.assertEqual(str(Worker("A", "B")), "A_B")
        self.assertEqual(str(Worker(None, "B")), "__B")
        self.assertEqual(str(Worker("A")), "A__")

    def test_has_free_hand(self):

        self.assertTrue(Worker(None, None).has_free_hand)
        self.assertTrue(Worker("A", None).has_free_hand)
        self.assertTrue(Worker(None, "B").has_free_hand)

        self.assertFalse(Worker("A", "B").has_free_hand)
        self.assertFalse(Worker("P", "B").has_free_hand)

    def test_has_finished_component(self):

        self.assertTrue(Worker(PRODUCT).has_finished_component)
        self.assertTrue(Worker(None, PRODUCT).has_finished_component)
        self.assertTrue(Worker(PRODUCT, None).has_finished_component)
        self.assertTrue(Worker(PRODUCT, "A").has_finished_component)
        self.assertTrue(Worker("B", PRODUCT).has_finished_component)

        self.assertFalse(Worker("A", "B").has_finished_component)
        self.assertFalse(Worker(None, "B").has_finished_component)
        self.assertFalse(Worker("A", None).has_finished_component)
        self.assertFalse(Worker(None, None).has_finished_component)

    def test_wants(self):

        self.assertTrue(Worker().wants(COMPONENT_A))
        self.assertTrue(Worker().wants(COMPONENT_B))
        self.assertTrue(Worker(COMPONENT_B).wants(COMPONENT_A))
        self.assertTrue(Worker(COMPONENT_A).wants(COMPONENT_B))
        self.assertTrue(Worker(PRODUCT).wants(COMPONENT_A))
        self.assertTrue(Worker(PRODUCT).wants(COMPONENT_B))

        self.assertFalse(Worker(COMPONENT_A, PRODUCT).wants(COMPONENT_B))
        self.assertFalse(Worker(COMPONENT_A).wants(COMPONENT_A))
        self.assertFalse(Worker().wants(PRODUCT))

    def test_acquire_component(self):

        worker = Worker()
        self.assertEqual(worker.construction_time_remaining, 0)

        worker.acquire_component(COMPONENT_A)
        self.assertEqual(worker.construction_time_remaining, 0)
        self.assertIn(COMPONENT_A, worker.holding)

        worker.acquire_component(COMPONENT_B)
        self.assertIn(COMPONENT_A, worker.holding)
        self.assertIn(COMPONENT_B, worker.holding)
        self.assertEqual(
            worker.construction_time_remaining, Worker.CONSTRUCTION_TIME)

    def test_return_finished_product(self):
        worker = Worker(PRODUCT)
        self.assertIn(PRODUCT, worker.holding)
        self.assertEqual(worker.return_finished_product(), PRODUCT)
        self.assertNotIn(PRODUCT, worker.holding)

    def test_tick(self):
        worker = Worker(COMPONENT_A, COMPONENT_B)
        self.assertEqual(worker.construction_time_remaining, 0)
        self.assertIn(COMPONENT_A, worker.holding)
        self.assertIn(COMPONENT_B, worker.holding)

        worker.tick()
        self.assertEqual(worker.construction_time_remaining, 0)
        self.assertIn(COMPONENT_A, worker.holding)
        self.assertIn(COMPONENT_B, worker.holding)

        worker.construction_time_remaining = 2
        worker.tick()
        self.assertEqual(worker.construction_time_remaining, 1)
        self.assertIn(COMPONENT_A, worker.holding)
        self.assertIn(COMPONENT_B, worker.holding)

        worker.tick()
        self.assertEqual(worker.construction_time_remaining, 0)
        self.assertNotIn(COMPONENT_A, worker.holding)
        self.assertNotIn(COMPONENT_B, worker.holding)
        self.assertIn(PRODUCT, worker.holding)


class BeltTestCase(TestCase):

    def test___init__(self):
        belt = Belt(5)
        self.assertEqual(len(belt.slots), 5)
        self.assertEqual(len(belt.workers), 5)

    def test_run(self):
        belt = Belt(5, loop_total=7)
        with mock.patch.object(belt, "on_tick") as m:
            belt.run()
            self.assertEqual(m.call_count, 7)

    def test_advance(self):
        belt = Belt(5)
        self.assertEqual(belt.tally["produced"], 0)
        self.assertEqual(belt.tally["wasted"], 0)
        self.assertEqual(len(belt.slots), 5)
        with mock.patch.object(belt, "_add_component") as m_ac:
            with mock.patch("random.randint", return_value=2):
                belt.advance()
                self.assertEqual(len(belt.slots), 5)
                self.assertEqual(m_ac.call_count, 0)
            with mock.patch("random.randint", return_value=1):
                belt.advance()
                self.assertEqual(len(belt.slots), 5)
                self.assertEqual(m_ac.call_count, 1)

class SlotOccupiedError(Exception):
    pass


class SlotEmptyError(Exception):
    pass


class Slot:
    """
    Think of this as a class for a space into which you put stuff on the belt.
    It can only hold one thing at a time and is manipulated with the hopefully
    familiar methods ``.push()`` and ``.pop()``.

    Assumption: We could go full police-state and have the slot be smart enough
    to decide what's appropriate for it to hold, but that felt overkill for
    this exercise.
    """

    def __init__(self, holding=None):
        self.holding = holding

    def __str__(self):
        return self.holding or " "

    def __bool__(self):
        return bool(self.holding)

    def push(self, item):
        """
        Put something into the slot.
        """

        if self.holding is not None:
            raise SlotOccupiedError()

        self.holding = item

    def pop(self):
        """
        Remove whatever is in the slot and return it.
        """
        if self.holding is None:
            raise SlotEmptyError()

        item = self.holding
        self.holding = None

        return item

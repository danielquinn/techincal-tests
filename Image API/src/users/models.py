from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils import timezone
from django.utils.functional import cached_property


class Size(models.Model):
    name = models.CharField(max_length=128)
    height = models.PositiveIntegerField(default=0)

    def __str__(self) -> str:
        return f"{self.name} ({self.height}px)"

    def save(self, *args, **kwargs):
        from images.tasks.thumbnailer import generate_thumbnails

        has_changed = False
        if self.pk:
            if Size.objects.get(pk=self.pk).height != self.height:
                has_changed = True

        super().save(*args, **kwargs)

        if not has_changed:
            return

        for thumbnail in self.thumbnails.all():
            generate_thumbnails.delay(
                image_id=thumbnail.base.pk,
                size_id=self.pk,
            )


class Tier(models.Model):
    name = models.CharField(max_length=128)
    sizes = models.ManyToManyField(Size, related_name="tiers")
    can_see_original = models.BooleanField(default=False)
    can_use_expiring_urls = models.BooleanField(default=False)

    def __str__(self) -> str:
        return self.name


class User(AbstractBaseUser, PermissionsMixin):
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["name"]

    email = models.EmailField(unique=True)
    name = models.CharField(max_length=128)
    tier = models.ForeignKey(Tier, on_delete=models.CASCADE)
    is_staff = models.BooleanField(
        "Staff status",
        default=False,
        help_text="Designates whether the user can log into this admin site.",
    )
    is_active = models.BooleanField(
        "Active",
        default=True,
        help_text=(
            "Designates whether this user should be treated as active. "
            "Deselect this instead of deleting accounts."
        ),
    )

    created = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(auto_now=True)

    objects = BaseUserManager()

    def __str__(self) -> str:
        return self.name

    def get_full_name(self) -> str:
        return str(self)

    def get_short_name(self) -> str:
        return self.email

    @cached_property
    def available_sizes(self):
        return self.tier.sizes.all()

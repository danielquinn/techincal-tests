from django.test import TestCase

from .factories import SizeFactory, TierFactory, UserFactory


class SizeTestCase(TestCase):
    def test___str__(self):
        self.assertEqual(str(SizeFactory(name="Test", height=5)), "Test (5px)")


class TierTestCase(TestCase):
    def test___str__(self):
        self.assertEqual(str(TierFactory(name="Test")), "Test")


class UserTestCase(TestCase):
    def test___str__(self):
        self.assertEqual(str(UserFactory(name="Test")), "Test")

    def test_get_full_name(self):
        self.assertEqual(UserFactory(name="Test").get_full_name(), "Test")

    def test_get_short_name(self):
        self.assertEqual(
            UserFactory(email="test@test.ca").get_short_name(), "test@test.ca"
        )

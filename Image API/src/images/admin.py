from django.contrib import admin

from .models import Image, Thumbnail


@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    list_display = ("uuid", "mimetype", "creator")


@admin.register(Thumbnail)
class ThumbnailAdmin(admin.ModelAdmin):
    list_display = ("uuid", "size")

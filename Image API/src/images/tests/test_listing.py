from django.test import TestCase
from django.urls import reverse

from rest_framework.status import HTTP_200_OK, HTTP_403_FORBIDDEN

from users.tests.factories import SizeFactory, TierFactory, UserFactory

from ..models import Thumbnail
from .factories import ImageFactory


class ListingTestCase(TestCase):
    """
    Users should be able to list their images.
    """

    def test_unauthenticated(self):
        # Act
        response = self.client.get(reverse("api-1:image-list"))

        # Assert
        self.assertEqual(response.status_code, HTTP_403_FORBIDDEN)

    def test_authenticated_shows_only_yours(self):
        # Arrange
        size_a = SizeFactory(height=100)

        tier_a = TierFactory()
        tier_a.sizes.add(size_a)

        tier_b = TierFactory()
        tier_b.sizes.add(size_a)

        user_a = UserFactory(tier=tier_a)
        user_b = UserFactory(tier=tier_b)

        image_a1 = ImageFactory(creator=user_a)
        image_a2 = ImageFactory(creator=user_a)

        ImageFactory(creator=user_b)

        thumbnails = Thumbnail.objects.filter(base__creator=user_a)
        expected_thumbnail_uuids = set(
            [str(_) for _ in thumbnails.values_list("uuid", flat=True)]
        )

        self.client.force_login(user_a)

        # Act
        response = self.client.get(reverse("api-1:image-list"))

        # Assert
        self.assertEqual(response.status_code, HTTP_200_OK)

        results = response.json()

        self.assertEqual(len(results), 2)
        self.assertEqual(
            {
                str(image_a1.uuid),
                str(image_a2.uuid),
            },
            set([_["uuid"] for _ in results]),
        )

        actual_thumbnail_uuids = set()
        for image in results:
            for thumbnail in image["thumbnails"]:
                actual_thumbnail_uuids.add(thumbnail["uuid"])
        self.assertEqual(expected_thumbnail_uuids, actual_thumbnail_uuids)

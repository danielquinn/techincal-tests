from pathlib import Path

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase

from PIL import Image as PillowImage

from users.tests.factories import SizeFactory, TierFactory, UserFactory

from ..models import Image, Thumbnail
from .factories import ImageFactory


class ThumbnailGenerationTestCase(TestCase):
    """
    There are three account tiers: Basic, Premium and Enterprise
    """

    def test_generation_for_all_tiers(self):
        # Arrange
        basic = TierFactory(name="Basic")
        premium = TierFactory(name="Premium")
        enterprise = TierFactory(name="Enterprise")

        size200 = SizeFactory(height=200)
        size400 = SizeFactory(height=400)

        basic.sizes.add(size200)
        premium.sizes.add(size200)
        premium.sizes.add(size400)
        enterprise.sizes.add(size200)
        enterprise.sizes.add(size400)

        user_a = UserFactory(tier=basic)
        user_b = UserFactory(tier=premium)
        user_c = UserFactory(tier=enterprise)

        ImageFactory(creator=user_a)
        ImageFactory(creator=user_b)
        ImageFactory(creator=user_c)

        # Assert
        self.assertEqual(Image.objects.count(), 3)
        self.assertEqual(Thumbnail.objects.count(), 5)

        thumbnails = Thumbnail.objects.all()
        self.assertEqual(thumbnails.filter(base__creator=user_a).count(), 1)
        self.assertEqual(thumbnails.filter(base__creator=user_b).count(), 2)
        self.assertEqual(thumbnails.filter(base__creator=user_c).count(), 2)

    def test_regeneration_when_size_changes(self):
        # Arrange
        basic = TierFactory(name="Basic")
        size = SizeFactory(height=10)
        basic.sizes.add(size)
        user = UserFactory(tier=basic)
        source_image = Path(__file__).parent / "data" / "original.jpg"
        with source_image.open("rb") as f:
            image = ImageFactory(
                creator=user,
                original=SimpleUploadedFile(
                    name="test.jpg",
                    content=f.read(),
                    content_type="image/jpeg",
                ),
            )

        thumbnail = image.thumbnails.first()
        self.assertEqual(Thumbnail.objects.count(), 1)
        self.assertEqual(PillowImage.open(thumbnail.file.path).size[1], 10)

        # Act
        size.height = 20
        size.save()
        size.save()  # Save a second time for coverage

        thumbnail = image.thumbnails.first()
        self.assertEqual(Thumbnail.objects.count(), 1)
        self.assertEqual(PillowImage.open(thumbnail.file.path).size[1], 20)

import factory

from users.tests.factories import UserFactory

from ..models import Image
from .helpers.files import JPG


class ImageFactory(factory.django.DjangoModelFactory):
    original = factory.django.FileField(filename="test.png", data=JPG)
    mimetype = "image/jpeg"
    creator = factory.SubFactory(UserFactory)

    class Meta:
        model = Image

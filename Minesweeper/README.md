# Minesweeper

Write a console-based Minesweeper game.

This was actually a project I did with a student to help them learn Python, and
I had so much fun doing it, I decided to roll it into this repo.  Currently, it
only draws the board complete with bombs & flags, but maybe one day we'll
extend it to support actually playing the game.

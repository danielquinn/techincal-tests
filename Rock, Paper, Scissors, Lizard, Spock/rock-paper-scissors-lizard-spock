#!/usr/bin/env python3

from __future__ import annotations

import sys

from argparse import ArgumentParser
from random import choice
from typing import List, Type


class CommandError(Exception):
    def __init__(self, message, code):
        super().__init__(message)
        self.code = code


class Choice:

    ICON = ""

    @property
    def beats(self) -> Tuple[Choice, Choice]:
        raise NotImplementedError()

    def __str__(self):
        return self.ICON

    def __gt__(self, other) -> bool:
        return isinstance(other, self.beats)

    def __eq__(self, other) -> bool:
        return self.__class__ == other.__class__

    @classmethod
    def build(cls, name):
        lookup = {c.__name__.lower(): c() for c in cls.__subclasses__()}
        return lookup[name]


class Rock(Choice):

    ICON = "🪨"

    @property
    def beats(self) -> Tuple[Type[Choice]]:
        return (Scissors, Lizard)


class Paper(Choice):

    ICON = "📰"

    @property
    def beats(self) -> Tuple[Type[Choice]]:
        return (Rock, Spock)


class Scissors(Choice):

    ICON = "✂️"

    @property
    def beats(self) -> Tuple[Type[Choice]]:
        return (Paper, Lizard)


class Lizard(Choice):

    ICON = "🐲"

    @property
    def beats(self) -> Tuple[Type[Choice]]:
        return (Paper, Spock)


class Spock(Choice):

    ICON = "🖖"

    @property
    def beats(self) -> Tuple[Type[Choice]]:
        return (Rock, Scissors)


class Command:

    CHOICES = {
        Rock.__name__.lower(): Rock,
        Paper.__name__.lower(): Paper,
        Scissors.__name__.lower(): Scissors,
        Lizard.__name__.lower(): Lizard,
        Spock.__name__.lower(): Spock,
    }

    def __init__(self):
        parser = ArgumentParser()
        parser.add_argument(
            "selected",
            choices=self.CHOICES.keys()
        )
        self.args = parser.parse_args()

    @property
    def choice(self) -> Type[Choice]:
        return self.CHOICES[self.args.selected]()

    def __call__(self) -> int:
        selected = Choice.build(self.args.selected)
        opponent = self.CHOICES[choice(tuple(self.CHOICES.keys()))]()
        print(f"You selected {selected}")
        print(f"Your opponent selected {opponent}")
        if self.choice > opponent:
            print("You win!")
        elif self.choice == opponent:
            print("It's a draw!")
        else:
            print("You lose :-(")
        return 0


def main():
    try:
        return Command()()
    except CommandError as e:
        print(e, file=sys.stderr)
        return e.code


if __name__ == "__main__":
    main()

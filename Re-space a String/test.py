#!/usr/bin/env python3

def is_word(string):
    return string in ("he", "hell", "or", "hello", "world")


class NotWordsError(Exception):
    pass


def get_words(string, n=0):

    if is_word(string[:n]):

        if len(string) == n - 1:
            return string[:n]

        try:
            remainder = get_words(string[n:])
        except NotWordsError:
            pass
        else:
            return string[:n] + " " + remainder

    elif len(string) == n - 1:
        raise NotWordsError()

    return get_words(string, n + 1)



assert get_words("helloworld") == "hello world"


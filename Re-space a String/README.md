# Re-Space a String

Given a string of arbitrary length containing an english sentence *with the
spaces removed*, return the string with the spaces restored.  For this
exercise you have a function called `is_word()` which returns True if a string
is an English word.

## Example

```python
def is_word(string):
    return string in ("he", "hell", "or", "hello", "world")


def get_words(...):
    ...


assert get_words("helloworld") == "hello world"
```


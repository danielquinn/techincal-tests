# Database Sharding & Rebalancing

* Restrict the time to maximum 2 hours (assume easier options if/when needed)
* If you're unsure about any requirements, please assume the easier option


## Challenge

* There is a database with Comments.
* The object Comment (id = number, text = string) will provide `save()` and
  `get_object_by_id(id=[number])` methods, which saves and retrieves the
  object from a database (see notes below).
* Every object will be saved in one of 4 tables (tables have the same
  structure and named like comment0, comment1, comment2, comment3) depending
  on the object id using the Modulus function, i.e.
  `ID % 4 = 0, 1, 2, or 3`.  Ex: If `id=317`, the object will be kept in DB
  number 1.
* Populate the tables with random comments (In such way that the tables are
  reasonable populated with approximately same number of records).
* Add two new tables: comment4 and comment5 and create code to rebalance
  (move) the objects according to new function: `ID % 6` (as some of them will
  be in the “wrong” table).
* **Extra**: Change the modulus function for a consistent hashing function.

## Notes

* The Extra section is only if you have time and/or personal interest.
* You can choose any persistent system or library (please keep it simple).
  Some ideas: SQLite or any simple file-persistence framework.

# Robot Warehouse

* Write a program for a robot that exists on an n x n grid.
    * The robot may not move beyond the grid.
    * You should offer orders to the robot in the form of "N" "E" "S" and "W".
        * These commands may come one at a time or as a string.
* This grid (warehouse) consists of crates that can be picked up, moved around, and dropped.
    * Only one crate may occupy a space at a time.


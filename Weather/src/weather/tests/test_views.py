from unittest import mock

from django.test import TestCase

from ..views import MetricListView


class RateLogHistoryViewTestCase(TestCase):

    @mock.patch("weather.views.generics.ListAPIView.get_serializer")
    def test_get_serializer(self, m):

        view = MetricListView()

        view.request = mock.Mock(GET={})
        view.get_serializer()
        self.assertNotIn("field_limiters", m.call_args[1])

        view.request = mock.Mock(GET={"fields": "a,b,c"})
        view.get_serializer()
        self.assertIn("field_limiters", m.call_args[1])
        self.assertEqual(m.call_args[1]["field_limiters"], ["a", "b", "c"])

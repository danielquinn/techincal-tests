from django_filters import rest_framework as filters
from django.views.generic import TemplateView
from rest_framework import generics, permissions
from rest_framework.renderers import BrowsableAPIRenderer, JSONRenderer

from .filters import MetricFilterSet
from .models import Metric
from .serializers import MetricSerializer


class IndexView(TemplateView):

    template_name = "weather/index.html"


class MetricListView(generics.ListAPIView):

    queryset = Metric.objects.order_by("-date").select_related("region")
    serializer_class = MetricSerializer
    permission_classes = (permissions.AllowAny,)
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = MetricFilterSet

    def get_serializer(self, *args, **kwargs):
        fields = self.request.GET.get("fields")
        if fields:
            kwargs["field_limiters"] = fields.split(",")
        return super().get_serializer(*args, **kwargs)

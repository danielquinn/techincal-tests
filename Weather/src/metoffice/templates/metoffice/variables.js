window.MetOffice = {
	urls: {
		api: {
			metrics: "{% url 'metrics' %}"
		}
	},
	regions: {{ regions|safe }}
};
#!/usr/bin/env python3


class Node(object):
    def __init__(self, value=None, nxt=None):
        self.value = value
        self.next = nxt


def reverse_linked_list(head):
    new_head = None
    while head:
        head.next = new_head
        head = head.next
        new_head = head
    return new_head


def reverse_linked_list_nondestructive(head):
    new_head = None
    while head:
        new_head = Node(head.value, nxt=new_head)
        head = head.next
    return new_head

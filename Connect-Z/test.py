#!/usr/bin/env python

import sys

from pathlib import Path
from typing import Tuple


class IllegalContinueError(Exception):
    CODE = 1


class IllegalMoveError(Exception):
    CODE = 2


class IllegalGameError(Exception):
    CODE = 3


class InvalidFileError(Exception):
    CODE = 4


class FileError(Exception):
    CODE = 5


class Board:
    def __init__(self, width: int, height: int, win_condition: int) -> None:
        self.width = width
        self.height = height
        self.win_condition = win_condition
        self.state = [[None for c in range(width)] for r in range(height)]

        if self.width < self.win_condition:
            if self.height < self.win_condition:
                raise IllegalGameError()

    @property
    def has_moves_available(self):
        for row in self.state:
            if None in row:
                return True
        return False

    def add_piece(self, x: int, team: str) -> Tuple[int, int]:

        if x >= self.width:
            raise IllegalMoveError()

        y = 0
        while y < self.height:
            if not self.is_occupied(x, y):
                self.state[y][x] = team
                return x, y
            y += 1

        raise IllegalMoveError()

    def render(self):
        """
        Not a part of the test, but this sure did make it easy to debug.
        """

        print("\n")
        print("===" * self.width)
        for y, row in reversed(list(enumerate(self.state))):
            for x, _ in enumerate(row):
                print(" {} ".format(_ or "_"), end="")
            print("")
        print("===" * self.width)

    def is_occupied(self, x: int, y: int) -> bool:
        return self.state[y][x] is not None

    def is_winner(self, x: int, y: int, team: str) -> bool:
        vectors = (("ne", "se"), ("nw", "sw"), ("e", "w"), ("s",))
        for vector in vectors:
            total = 1
            for heading in vector:
                total += self.check_adjacent(x, y, heading, team)
            if total >= self.win_condition:
                return True
        return False

    def check_adjacent(
        self, x: int, y: int, heading: str, team: str, count: int = 0
    ) -> int:

        lookup = {
            "ne": (x - 1, y + 1),
            "e": (x - 1, y),
            "se": (x - 1, y - 1),
            "s": (x, y - 1),
            "sw": (x + 1, y - 1),
            "w": (x + 1, y),
            "nw": (x + 1, y + 1),
        }
        target_x = lookup[heading][0]
        target_y = lookup[heading][1]

        # Negative numbers will cause wrapping, which isn't what we want.
        if target_x < 0 or target_y < 0:
            return count

        try:
            if self.state[target_y][target_x] == team:
                return self.check_adjacent(
                    target_x, target_y, heading, team, count + 1
                )
        except IndexError:
            pass  # Target is out of bounds

        return count


class Game:
    def __init__(self, spec: Path):

        try:
            with open(spec) as f:
                width, height, win_condition = f.readline().strip().split(" ")
                self.moves = [int(i) for i in f.readlines() if i]
        except OSError:
            raise FileError()
        except ValueError:
            raise InvalidFileError()

        self.winner = None
        self.board = Board(
            width=int(width),
            height=int(height),
            win_condition=int(win_condition),
        )

    def play(self):
        players = ("a", "b")

        for index, move in enumerate(self.moves):

            if self.winner:
                raise IllegalContinueError()

            player = players[index % 2]
            x, y = self.board.add_piece(move - 1, player)
            if self.board.is_winner(x, y, player):
                return player


def main():

    try:
        game = Game(Path("spec.txt"))
    except (FileError, InvalidFileError) as e:
        sys.stderr.write("Unable to read the input file")
        sys.exit(e.CODE)
    except IllegalGameError as e:
        sys.stderr.write(
            "The dimensions of the game prohibit anyone from winning."
        )
        sys.exit(e.CODE)

    try:
        if winner := game.play():
            sys.stdout.write(f"The winner is Team {winner}!")
    except IllegalMoveError as e:
        sys.stderr.write("An illegal move was detected.")
        sys.exit(e.CODE)

    game.board.render()


if __name__ == "__main__":
    main()

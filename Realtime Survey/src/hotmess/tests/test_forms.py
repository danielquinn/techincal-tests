from django import forms
from django.forms.widgets import RadioSelect
from django.test import TestCase
from hotmess.forms import BootstrapMixin


class BootStrapMixinTestCase(TestCase):

    def test__bootstrapify(self):

        class TestForm(BootstrapMixin, forms.Form):
            char = forms.CharField(max_length=7)
            choice_select = forms.ChoiceField()
            choice_radio = forms.ChoiceField(widget=RadioSelect)
            boolean = forms.BooleanField()
            email = forms.EmailField()

            def __init__(self, *args, **kwargs):
                forms.Form.__init__(self, *args, **kwargs)
                self._bootstrapify()

        form = TestForm()
        for f in ("char", "choice_select", "email"):
            self.assertEqual(
                form.fields[f].widget.attrs["class"],
                "form-control"
            )

        for f in ("boolean", "choice_radio"):
            self.assertNotIn("class", form.fields[f].widget.attrs)

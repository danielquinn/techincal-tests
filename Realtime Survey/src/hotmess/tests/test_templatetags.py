from unittest import mock

from django.test import TestCase

from hotmess.templatetags.sexy import random_background


class RandomBackgroundTestCase(TestCase):

    @mock.patch("hotmess.templatetags.sexy.randint", return_value=7)
    @mock.patch("hotmess.templatetags.sexy.static")
    def test_random_background(self, m_static, m_randint):
        random_background()
        self.assertEqual(m_static.call_count, 1)
        self.assertEqual(
            m_static.call_args[0][0], "hotmess/img/backgrounds/07.jpg")

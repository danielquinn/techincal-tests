from channels.routing import route
from surveys.consumers import socket_connect, socket_disconnect

channel_routing = [
    route("websocket.connect", socket_connect),
    route("websocket.disconnect", socket_disconnect),
]

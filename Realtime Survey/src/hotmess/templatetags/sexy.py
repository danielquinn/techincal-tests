from random import randint

from django import template
from django.contrib.staticfiles.templatetags.staticfiles import static

register = template.Library()


@register.simple_tag
def random_background():
    return static("hotmess/img/backgrounds/{:02}.jpg".format(randint(0, 11)))

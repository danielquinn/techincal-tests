from django.contrib.auth.models import User
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    """
    Used at container start time to create a known admin user for the demo.
    """

    PASSWORD = "Correct horse battery staple"  # XKCD FTW

    def handle(self, *args, **options):

        if User.objects.all():
            return

        print('Initialising admin user (password: "{}")'.format(self.PASSWORD))

        user = User.objects.create(
            username="admin", is_staff=True, is_superuser=True)
        user.set_password(self.PASSWORD)
        user.save()

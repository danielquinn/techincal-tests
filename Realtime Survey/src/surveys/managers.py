from django.db import models


class SurveyQuerySet(models.query.QuerySet):
    def completed(self):
        return self._clone().filter(
            stage1_completed=True,
            stage2_completed=True,
            stage3_completed=True,
            stage4_completed=True
        )


class SurveyManager(models.Manager):

    def get_queryset(self):
        return SurveyQuerySet(self.model, using=self._db)

    def completed(self):
        return self.get_queryset().completed()

from django.conf.urls import url
from django.contrib import admin
from django.views.generic import TemplateView

from .views import (
    SurveyStage1View,
    SurveyStage2View,
    SurveyStage3View,
    SurveyStage4View
)

urlpatterns = [
    url(r"^$", SurveyStage1View.as_view(), name="stage-1"),
    url(r"^2$", SurveyStage2View.as_view(), name="stage-2"),
    url(r"^3$", SurveyStage3View.as_view(), name="stage-3"),
    url(r"^4$", SurveyStage4View.as_view(), name="stage-4"),
    url(r"^thanks$", TemplateView.as_view(template_name="surveys/thanks.html"), name="thanks"),
]

import factory

from ..models import Survey


class SurveyFactory(factory.DjangoModelFactory):

    class Meta:
        model = Survey

    name = factory.Faker("name")
    email = factory.Faker("email")


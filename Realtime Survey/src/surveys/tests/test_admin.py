from unittest.mock import Mock

from django.test import TestCase

from ..admin import SurveyAdmin
from ..models import Survey
from .factories import SurveyFactory


class SurveyAdminTestCase(TestCase):

    def test__get_colours(self):

        m = Mock(spec=SurveyAdmin)

        kwargs = {
            "stage1_completed": True,
            "stage2_completed": True,
            "stage3_completed": True,
            "stage4_completed": True,
        }
        SurveyFactory.create(
            colours=[Survey.COLOUR_RED, Survey.COLOUR_BLUE], **kwargs)
        SurveyFactory.create(
            colours=[Survey.COLOUR_RED, Survey.COLOUR_GREY], **kwargs)
        SurveyFactory.create(
            colours=[Survey.COLOUR_RED, Survey.COLOUR_BLUE], **kwargs)

        self.assertEqual(SurveyAdmin._get_colours(m), {
            Survey.COLOUR_RED: 3,
            Survey.COLOUR_BLUE: 2,
            Survey.COLOUR_GREY: 1
        })

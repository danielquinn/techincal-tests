from django.test import TestCase

from .factories import SurveyFactory
from ..models import Survey


class SurveyManagerTestCase(TestCase):

    def setUp(self):
        SurveyFactory.create()
        SurveyFactory.create(
            stage1_completed=True
        )
        SurveyFactory.create(
            stage1_completed=True,
            stage2_completed=True
        )
        SurveyFactory.create(
            stage1_completed=True,
            stage2_completed=True,
            stage3_completed=True
        )
        SurveyFactory.create(
            stage1_completed=True,
            stage2_completed=True,
            stage3_completed=True,
            stage4_completed=True
        )

    def test_basics(self):
        self.assertEqual(Survey.objects.all().count(), 5)
        self.assertEqual(Survey.objects.filter(stage1_completed=True).count(), 4)
        self.assertEqual(Survey.objects.filter(stage2_completed=True).count(), 3)
        self.assertEqual(Survey.objects.filter(stage3_completed=True).count(), 2)
        self.assertEqual(Survey.objects.filter(stage4_completed=True).count(), 1)

    def test_completed(self):
        completed = Survey.objects.completed()
        self.assertEqual(completed.count(), 1)
        self.assertEqual(completed.filter(stage1_completed=True).count(), 1)
        self.assertEqual(completed.filter(stage2_completed=True).count(), 1)
        self.assertEqual(completed.filter(stage3_completed=True).count(), 1)
        self.assertEqual(completed.filter(stage4_completed=True).count(), 1)

var $ = django.jQuery;
var Survey = function(){

	var self = this;

	var $results = $("#result_list tbody");
	var $colourbox = $('<div class="colourbox"></div>');

	var $ageTable = $("table.age");
	var $genderTable = $("table.gender");
	var $colourTable = $("table.colour");

	var genderMap = {
		"?": "",
		"m": "Male",
		"f": "Female",
		"o": "It's complicated"
	};

	this.l = {  // Lookup table so we're not constantly searching
		ages: {
			avg: $(".average", $ageTable),
			min: $(".min", $ageTable),
			max: $(".max", $ageTable)
		},
		genders: {
			unknown: {
				men: $(".unknown .men", $genderTable),
				women: $(".unknown .women", $genderTable),
				other: $(".unknown .other", $genderTable),
				all: $(".unknown .all", $genderTable)
			},
			men: {
				unknown: $(".men .unknown", $genderTable),
				women: $(".men .women", $genderTable),
				other: $(".men .other", $genderTable),
				all: $(".men .all", $genderTable)
			},
			women: {
				unknown: $(".women .unknown", $genderTable),
				men: $(".women .men", $genderTable),
				other: $(".women .other", $genderTable),
				all: $(".women .all", $genderTable)
			},
			other: {
				unknown: $(".other .unknown", $genderTable),
				men: $(".other .men", $genderTable),
				women: $(".other .women", $genderTable),
				all: $(".other .all", $genderTable)
			},
			all: {
				unknown: $(".all .unknown", $genderTable),
				men: $(".all .men", $genderTable),
				women: $(".all .women", $genderTable),
				other: $(".all .other", $genderTable)
			}
		},
		colours: [
			$(".top1", $colourTable),
			$(".top2", $colourTable),
			$(".top3", $colourTable)
		]
	};

	this.ages = null;
	this.genders = null;
	this.colours = null;

	this.colourChart = null;

	this.init = function(ages, genders, colours){

		self.ages = ages;
		self.genders = genders;
		self.colours = colours;

		$(".field-colours", $results).each(self.boxifyColours);

		self.setupListener();
		self.writeStats();

		self.drawColourChart();

	};

	/**
	 *
	 * Plug into the channels system to get new survey data and act on it
	 * accordingly.
	 *
	 */
	this.setupListener = function(){

		var socket = new WebSocket(window.location.protocol.replace("http", "ws") + "//" + window.location.host + "/updates/");

		socket.onmessage = function(e) {

			var survey = JSON.parse(e.data);
			var $row = $("input[name='_selected_action'][value='" + survey.id + "']").closest("tr");

			if ($row.length) {
				self.populateRow($row, survey);
			} else {
				self.createRow(survey);
			}

			// Is the survey complete?
			if (survey.colours && survey.colours.length){
				self.appendStatistics(survey);
				self.writeStats();
				self.drawColourChart();
			}

		};

		if (socket.readyState === WebSocket.OPEN) {
			socket.onopen();
		}

	};

	// DOM Manipulation -----------------------------------------------------

	/**
	 *
	 * Make pretty boxes out of boring hex values.
	 *
	 */
	this.boxifyColours = function() {

		var $this = $(this);
		var colours = $this.html().replace(/(\s|&nbsp;)+/g, "").split(",");

		$this.html("");
		for (var i = 0; i < colours.length; i++) {
			if (colours[i] === "") {
				continue;
			}
			$this.append($colourbox.clone().css("background-color", "#" + colours[i]));
		}

	};

	/**
	 *
	 * Populate a target `$row` with whatever you find in a `survey` object.
	 *
	 */
	this.populateRow = function($row, survey) {

		var fields = ["name", "email", "age", "about", "address", "book", "colours"];
		for (var i = 0; i < fields.length; i++) {
			var value = survey[fields[i]];
			if (fields[i] === "gender") {
				value = genderMap[value];
			}
			$row.find(".field-" + fields[i]).html(survey[fields[i]]);
		}

		if (survey.gender) {
			$row.find(".field-gender_").html(genderMap[survey.gender]);
		}

		if (survey.colours && survey.colours.length) {
			var $colours = $(".field-colours", $row);
			$colours.html(survey.colours.join(", "));
			$(".field-is_completed", $row).html("True");
			self.boxifyColours.call($colours);
		}

	};

	/**
	 *
	 * Create a new row for the table.  This only happens when a new survey
	 * is created, and the only data we *know* will be in there is the first
	 * two values.  Everything else is left blank to see if populateRow() can
	 * add to it.
	 *
	 */
	this.createRow = function(survey) {
		var $row = $('<tr class="row1">' +
			'<td class="action-checkbox">' +
			'<input name="_selected_action" value="' + survey.id + '" class="action-select" type="checkbox"></td>' +
			'<th class="field-name"><a href="/admin/surveys/survey/' + survey.id + '/change/">' + survey.name + '</a></th>' +
			'<td class="field-email">' + survey.email + '</td>' +
			'<td class="field-age"></td>' +
			'<td class="field-about"></td>' +
			'<td class="field-address"></td>' +
			'<td class="field-gender"></td>' +
			'<td class="field-book"></td>' +
			'<td class="field-colours"></td>' +
			'<td class="field-is_completed"></td>' +
			'</tr>');
		self.populateRow($row, survey);
		$results.prepend($row);
	};

	// Statistics Management ------------------------------------------------

	/**
	 *
	 * The statistics data needs to be updated once a survey is finished so
	 * we can recalculate everything in the summary.
	 *
	 */
	this.appendStatistics = function(survey){

		self.ages.push(survey.age);
		self.genders.push(survey.gender);
		for (var i = 0; i < survey.colours.length; i++) {
			var c = survey.colours[i];
			if (self.colours[c]) {
				self.colours[c]++;
			} else {
				self.colours[c] = 1;
			}
		}

	};

	// Summary Writers ------------------------------------------------------

	this.writeStats = function(){
		self.writeAgeData();
		self.writeGenderData();
		self.writeColourData();
	};

	this.writeAgeData = function(){

		if (!self.ages.length){
			return;
		}

		self.l.ages.avg.html(
			Math.round((self.ages.reduce(function(sum, value) {return sum + value;}, 0) / self.ages.length) * 100) / 100
		);
		self.l.ages.max.html(self.ages.reduce(function(a, b) {return Math.max(a, b);}));
		self.l.ages.min.html(self.ages.reduce(function(a, b) {return Math.min(a, b);}));

	};

	this.writeGenderData = function(){

		if (!self.genders.length){
			return;
		}

		var totalUnknown = self.countGenderOccurances("?");
		var totalMen = self.countGenderOccurances("m");
		var totalWomen = self.countGenderOccurances("f");
		var totalOther = self.countGenderOccurances("o");
		var totalAll = self.genders.length;

		self.setGenderField(self.l.genders.unknown.men, totalUnknown, totalMen);
		self.setGenderField(self.l.genders.unknown.women, totalUnknown, totalWomen);
		self.setGenderField(self.l.genders.unknown.other, totalUnknown, totalOther);
		self.setGenderField(self.l.genders.unknown.all, totalUnknown, totalAll);

		self.setGenderField(self.l.genders.men.unknown, totalMen, totalUnknown);
		self.setGenderField(self.l.genders.men.women, totalMen, totalWomen);
		self.setGenderField(self.l.genders.men.other, totalMen, totalOther);
		self.setGenderField(self.l.genders.men.all, totalMen, totalAll);

		self.setGenderField(self.l.genders.women.unknown, totalWomen, totalUnknown);
		self.setGenderField(self.l.genders.women.men, totalWomen, totalMen);
		self.setGenderField(self.l.genders.women.other, totalWomen, totalOther);
		self.setGenderField(self.l.genders.women.all, totalWomen, totalAll);

		self.setGenderField(self.l.genders.other.unknown, totalOther, totalUnknown);
		self.setGenderField(self.l.genders.other.men, totalOther, totalMen);
		self.setGenderField(self.l.genders.other.women, totalOther, totalWomen);
		self.setGenderField(self.l.genders.other.all, totalOther, totalAll);

		self.setGenderField(self.l.genders.all.unknown, totalAll, totalUnknown);
		self.setGenderField(self.l.genders.all.men, totalAll, totalMen);
		self.setGenderField(self.l.genders.all.women, totalAll, totalWomen);
		self.setGenderField(self.l.genders.all.other, totalAll, totalOther);

	};

	/**
	 *
	 * Do some math on the genders array to see how many `key` references
	 * there are in there.
	 *
	 */
	this.countGenderOccurances = function(key){
		return self.genders.reduce(function(sum, value) {return (value === key) ? sum + 1 : sum;}, 0);
	};

	this.setGenderField = function($field, v1, v2) {
		if (v1 === 0 || v2 === 0) {
			$field.html("0");
		} else {
			$field.html(Math.round((v1 / v2) * 100) / 100);
		}
	};

	this.writeColourData = function(){
		var topColours = Object.keys(self.colours).sort(function(a, b){return self.colours[b] - self.colours[a]}).slice(0, 3);
		for (var i = 0; i < topColours.length; i++) {
			self.l.colours[i].css("background-color", "#" + topColours[i]);
		}
	};

	this.drawColourChart = function(){

		var data = [];
		var colours = {};
		for (var key in self.colours) {
			if (self.colours.hasOwnProperty(key)) {
				data.push([key, self.colours[key]]);
				colours[key] = "#" + key;
			}
		}

		if (self.colourChart) {
			self.colourChart.load({
				columns: data,
				colors: colours
			});
			return;
		}

		self.colourChart = c3.generate({
			bindto: "#colour-chart",
			legend: {
				hide: true
			},
			data: {
				columns: data,
				colors: colours,
				type : "pie"
			}
		});

	};

	return this;

};

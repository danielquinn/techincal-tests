import json
from urllib.parse import urlparse

from channels import Group
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.views.generic import FormView

from .forms import (
    SurveyFormStage1,
    SurveyFormStage2,
    SurveyFormStage3,
    SurveyFormStage4
)
from .models import Survey
from .settings import CHANNEL_UPDATES


class SurveyView(FormView):

    model = Survey

    def __init__(self):
        FormView.__init__(self)
        self._survey = None

    def dispatch(self, request, *args, **kwargs):
        """
        Generally, I don't like overriding .dispatch(), but doing so here meant
        that I could keep this mess away from everything else.

        This whole method is a result of the rules around bouncing users to the
        right form stage in the requirements doc.  Here, we're carrying around
        a survey instance from stage to stage, and the bit inside the `if`
        block handles redirecting the user to the right page.

        Basically, this will end one of two ways: a redirect to wherever you're
        supposed to be, or the form stage you asked for.
        """

        survey_id = request.session.get("survey")

        if survey_id:

            referrer = urlparse(request.META.get("HTTP_REFERER")).path
            prev_url = self._get_previous_url()
            next_url = self._get_next_url()

            self._survey = Survey.objects.get(pk=survey_id)

            if self._survey.is_completed:
                return HttpResponseRedirect(reverse("surveys:thanks"))

            if request.method == "GET":

                if not self._check_previous_completed():
                    return HttpResponseRedirect(prev_url)

                if self._check_this_completed():
                    if referrer not in (prev_url, next_url):
                        return HttpResponseRedirect(next_url)

        elif not self.STAGE == 1:
            return HttpResponseRedirect(reverse("surveys:stage-1"))

        return FormView.dispatch(self, request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = FormView.get_form_kwargs(self)
        kwargs["instance"] = self._survey
        return kwargs

    def get_context_data(self, **kwargs):
        context = FormView.get_context_data(self, **kwargs)
        if self.STAGE > 1:
            context["previous"] = self._get_previous_url()
        return context

    def form_valid(self, form):
        """
        Whenever a form is completed, we update the survey in the db, cache it
        in the session, and push an update out to Channels so the admin can
        have that auto-updating thingy.
        """

        self._survey = form.save()
        self.request.session["survey"] = self._survey.pk

        Group(CHANNEL_UPDATES).send({
            "text": json.dumps({
                "id": self._survey.pk,
                "name": self._survey.name,
                "email": self._survey.email,
                "age": self._survey.age,
                "about": self._survey.about,
                "address": self._survey.address,
                "gender": self._survey.gender,
                "book": self._survey.book,
                "colours": self._survey.colours,
            }, separators=(",", ":"))
        })

        return FormView.form_valid(self, form)

    def get_success_url(self):
        return self._get_next_url()

    def _check_previous_completed(self):
        """
        Return whether the previous was completed.
        """
        return getattr(
            self._survey, "stage{}_completed".format(self.STAGE - 1))

    def _check_this_completed(self):
        """
        Return whether this stage was already completed
        """
        return getattr(self._survey, "stage{}_completed".format(self.STAGE))

    def _get_previous_url(self):
        return reverse("surveys:stage-{}".format(self.STAGE - 1))

    def _get_next_url(self):
        return reverse("surveys:stage-{}".format(self.STAGE + 1))


class SurveyStage1View(SurveyView):

    STAGE = 1

    form_class = SurveyFormStage1
    template_name = "surveys/survey-1.html"

    def _get_previous_url(self):
        return None

    def _check_previous_completed(self):
        """
        There's no need for this sort of logic in stage 1
        """
        return True


class SurveyStage2View(SurveyView):

    STAGE = 2

    form_class = SurveyFormStage2
    template_name = "surveys/survey-2.html"


class SurveyStage3View(SurveyView):

    STAGE = 3

    form_class = SurveyFormStage3
    template_name = "surveys/survey-3.html"


class SurveyStage4View(SurveyView):

    STAGE = 4

    form_class = SurveyFormStage4
    template_name = "surveys/survey-4.html"

    def _get_next_url(self):
        return reverse_lazy("surveys:thanks")

from django.contrib.postgres.fields import ArrayField
from django.db import models

from .managers import SurveyManager


class Survey(models.Model):

    GENDER_UNKNOWN = "?"
    GENDER_FEMALE = "f"
    GENDER_MALE = "m"
    GENDER_OTHER = "o"
    GENDERS = (
        (GENDER_UNKNOWN, "Prefer not to say"),
        (GENDER_FEMALE, "Female"),
        (GENDER_MALE, "Male"),
        (GENDER_OTHER, "It's complicated")
    )

    COLOUR_GREY = "727272"
    COLOUR_RED = "F1595F"
    COLOUR_GREEN = "79C36A"
    COLOUR_BLUE = "599AD3"
    COLOUR_PEACH = "F9A65A"
    COLOUR_PURPLE = "9E66AB"
    COLOUR_BROWN = "CD7058"
    COLOUR_PINK = "D77FB3"
    COLOURS = (
        (COLOUR_GREY, "Grey"),
        (COLOUR_RED, "Red"),
        (COLOUR_GREEN, "Green"),
        (COLOUR_BLUE, "Blue"),
        (COLOUR_PEACH, "Peach"),
        (COLOUR_PURPLE, "Purple"),
        (COLOUR_BROWN, "Brown"),
        (COLOUR_PINK, "Pink"),
    )

    AGES = tuple((i, i) for i in range(12, 123))

    name = models.CharField(max_length=256)
    email = models.EmailField()

    age = models.PositiveIntegerField(choices=AGES, blank=True, null=True)
    about = models.TextField(blank=True)

    address = models.CharField(max_length=1024, blank=True)
    gender = models.CharField(
        max_length=1, choices=GENDERS, default=GENDER_UNKNOWN)

    book = models.CharField(max_length=1024, blank=True)
    colours = ArrayField(
        models.CharField(max_length=6, choices=COLOURS, blank=True),
        blank=True,
        default=list
    )

    stage1_completed = models.BooleanField(default=False)
    stage2_completed = models.BooleanField(default=False)
    stage3_completed = models.BooleanField(default=False)
    stage4_completed = models.BooleanField(default=False)

    objects = SurveyManager()

    def __str__(self):
        return self.name

    @property
    def is_completed(self):
        return self.stage1_completed and \
               self.stage2_completed and \
               self.stage3_completed and \
               self.stage4_completed

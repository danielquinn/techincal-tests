# Realtime Survey (Hotmess)

The task: Build a survey front-end and backend that sends results to the server
and have them show up in the admin in real-time.


## Library Explanations

Generally, I don't like to write code I don't have to, and thankfully, this job
asked for some very common stuff, so there's lots of tools out there that
shaved time & complexity of the task.

### Python

#### Django

This is a web app, and Django is the dominant Python web for a reason.  It
comes with templating, forms, models, database abstration, and an admin... for
free.  It'll let me keep most of my code declarative, which limits bugs and
the amount of code I need to test.

#### Channels

It's Django's answer to websockets and async web communcation and it's pretty
awesome.

#### django-extensions

I just find it handy.  Typically I don't use it for anything in production, but
for development, many of its features, like `shell_plus` make tinkering with
the project easier.

#### psycopg2

Necessary to talk to PostgreSQL


### Front-end

#### jQuery

It's what I know, and it's easier to use than just plain Javascript.

#### D3 & C3

D3 is a fabulous library for sexy visuals, and C3 makes it easy.

#### Bootstrap

I like things to be pretty and I generally lack the skills to do that.


### Server Side

#### Docker & docker-compose

Because I like my projects to be compartmentalised.

#### PostgreSQL

It's free, it's powerful, and it doesn't silently mangle your data like MySQL.

#### Redis

Normally, I wouldn't bring in a tool like this for such a small project, but
Channels needed a central store for message passing, and this was the
recommended tool.
